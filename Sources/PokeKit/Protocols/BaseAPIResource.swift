//
//  BaseAPIResource.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public protocol BaseAPIResource {
    associatedtype Resource: Codable
    var url: URL { get }
}

public extension BaseAPIResource {
    var id: Int {
        Int(url.lastPathComponent) ?? 0
    }

    var linksTo: String {
        url.deletingLastPathComponent().lastPathComponent
    }
}

@available(macOS 12.0.0, iOS 15.0.0, *)
extension BaseAPIResource {
    func fetch() async throws -> Resource {
        try await PokeAPI.shared.fetchAPIResource(self)
    }
}


