//
//  PokeAPI.swift
//
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public class PokeAPI {
    public static var shared = PokeAPI()
    public static var baseURL = URL(string: "https://pokeapi.co/api/v2")!

    private var session = URLSession.shared
    private var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    public enum Endpoints: String {
        case berry
        case berryFirmness = "berry-firmness"
        case berryFlavor = "berry-flavor"

        case contestType = "contest-type"
        case contestEffect = "contest-effect"
        case superContestEffect = "super-contest-effect"

        case encounterMethod = "encounter-method"
        case encounterCondition = "encounter-condition"
        case encounterConditionValue = "encounter-condition-value"

        case evolutionChain = "evolution-chain"
        case evolutionTrigger = "evolution-trigger"

        case generation
        case pokedex
        case version
        case versionGroup = "version-group"

        case item
        case itemAttribute = "item-attribute"
        case itemCategory = "item-category"
        case itemFlingEffect = "item-fling-effect"
        case itemPocket = "item-pocket"

        case location
        case locationArea = "location-area"
        case palParkArea = "pal-park-area"
        case region

        case machine

        case move
        case moveAilment = "move-ailment"
        case moveBattleStyle = "move-battle-style"
        case moveCategory = "move-category"
        case moveDamageClass = "move-damage-class"
        case moveLearnMethod = "move-learn-method"
        case moveTarget = "move-target"

        case ability
        case characteristic
        case eggGroup = "egg-group"
        case gender
        case growthRate = "growth-rate"
        case nature
        case pokeathlonStat = "pokeathlon-stat"
        case pokemon
        case pokemonColor = "pokemon-color"
        case pokemonForm = "pokemon-form"
        case pokemonHabitat = "pokemon-habitat"
        case pokemonShape = "pokemon-shape"
        case pokemonSpecies = "pokemon-species"
        case stat
        case type

        var url: URL {
            return baseURL.appendingPathComponent(rawValue)
        }
    }
    private init() {}
}

//Async/Await
@available(iOS 15.0, macOS 12.0.0, *)
public extension PokeAPI {

    //MARK: - Berries
    func fetchBerry(id: Int) async throws -> Berry {
        try await fetchAndDecode(from: .berry, parameter: id)
    }

    func fetchBerry(name: String) async throws -> Berry {
        try await fetchAndDecode(from: .berry, parameter: name)
    }

    func fetchBerryFirmness(id: Int) async throws -> BerryFirmness {
        try await fetchAndDecode(from: .berryFirmness, parameter: id)
    }

    func fetchBerryFirmness(name: String) async throws -> BerryFirmness {
        try await fetchAndDecode(from: .berryFirmness, parameter: name)
    }

    func fetchBerryFlavor(id: Int) async throws -> BerryFlavor {
        try await fetchAndDecode(from: .berryFlavor, parameter: id)
    }

    func fetchBerryFlavor(name: String) async throws -> BerryFlavor {
        try await fetchAndDecode(from: .berryFlavor, parameter: name)
    }

    //MARK: - Contests
    func fetchContestType(id: Int) async throws -> ContestType {
        try await fetchAndDecode(from: .contestType, parameter: id)
    }

    func fetchContestType(name: String) async throws -> ContestType {
        try await fetchAndDecode(from: .contestType, parameter: name)
    }

    func fetchContestEffect(id: Int) async throws -> ContestEffect {
        try await fetchAndDecode(from: .contestEffect, parameter: id)
    }

    func fetchContestEffect(name: String) async throws -> ContestEffect {
        try await fetchAndDecode(from: .contestEffect, parameter: name)
    }

    func fetchSuperContestEffect(id: Int) async throws -> SuperContestEffect {
        try await fetchAndDecode(from: .superContestEffect, parameter: id)
    }

    func fetchSuperContestEffect(name: String) async throws -> SuperContestEffect {
        try await fetchAndDecode(from: .superContestEffect, parameter: name)
    }

    func fetchEncounterMethod(id: Int) async throws -> EncounterMethod {
        try await fetchAndDecode(from: .encounterMethod, parameter: id)
    }


    //MARK: - Encounters
    func fetchEncounterMethod(name: String) async throws -> EncounterMethod {
        try await fetchAndDecode(from: .encounterMethod, parameter: name)
    }

    func fetchEncounterCondition(id: Int) async throws -> EncounterCondition {
        try await fetchAndDecode(from: .encounterCondition, parameter: id)
    }

    func fetchEncounterCondition(name: String) async throws -> EncounterCondition {
        try await fetchAndDecode(from: .encounterCondition, parameter: name)
    }

    func fetchEncounterConditionValue(id: Int) async throws -> EncounterConditionValues {
        try await fetchAndDecode(from: .encounterConditionValue, parameter: id)
    }

    func fetchEncounterConditionValue(name: String) async throws -> EncounterConditionValues {
        try await fetchAndDecode(from: .encounterConditionValue, parameter: name)
    }

    //MARK: - Evolution
    func fetchEvolutionChain(id: Int) async throws -> EvolutionChain {
        try await fetchAndDecode(from: .evolutionChain, parameter: id)
    }

    func fetchEvolutionTrigger(id: Int) async throws -> EvolutionTrigger {
        try await fetchAndDecode(from: .evolutionTrigger, parameter: id)
    }

    //MARK: - Games
    func fetchGeneration(id: Int) async throws -> Generation {
        try await fetchAndDecode(from: .generation, parameter: id)
    }

    func fetchGeneration(name: String) async throws -> Generation {
        try await fetchAndDecode(from: .generation, parameter: name)
    }

    func fetchPokedex(id: Int) async throws -> Pokedex {
        try await fetchAndDecode(from: .pokedex, parameter: id)
    }

    func fetchPokedex(name: String) async throws -> Pokedex {
        try await fetchAndDecode(from: .pokedex, parameter: name)
    }

    func fetchVersion(id: Int) async throws -> Version {
        try await fetchAndDecode(from: .version, parameter: id)
    }

    func fetchVersion(name: String) async throws -> Version {
        try await fetchAndDecode(from: .version, parameter: name)
    }

    func fetchVersionGroup(id: Int) async throws -> VersionGroup {
        try await fetchAndDecode(from: .versionGroup, parameter: id)
    }

    func fetchVersionGroup(name: String) async throws -> VersionGroup {
        try await fetchAndDecode(from: .versionGroup, parameter: name)
    }

    //MARK: - Items
    func fetchItem(id: Int) async throws -> Item {
        try await fetchAndDecode(from: .item, parameter: id)
    }

    func fetchItem(name: String) async throws -> Item {
        try await fetchAndDecode(from: .item, parameter: name)
    }

    func fetchItemAttribute(id: Int) async throws -> ItemAttribute {
        try await fetchAndDecode(from: .itemAttribute, parameter: id)
    }

    func fetchItemAttribute(name: String) async throws -> ItemAttribute {
        try await fetchAndDecode(from: .itemAttribute, parameter: name)
    }

    func fetchItemCategory(id: Int) async throws -> ItemCategory {
        try await fetchAndDecode(from: .itemCategory, parameter: id)
    }

    func fetchItemCategory(name: String) async throws -> ItemCategory {
        try await fetchAndDecode(from: .itemCategory, parameter: name)
    }

    func fetchItemFlingEffect(id: Int) async throws -> ItemFlingEffect {
        try await fetchAndDecode(from: .itemFlingEffect, parameter: id)
    }

    func fetchItemFlingEffect(name: String) async throws -> ItemFlingEffect {
        try await fetchAndDecode(from: .itemFlingEffect, parameter: name)
    }

    func fetchItemPocket(id: Int) async throws -> ItemPocket {
        try await fetchAndDecode(from: .itemPocket, parameter: id)
    }

    func fetchItemPocket(name: String) async throws -> ItemPocket {
        try await fetchAndDecode(from: .itemPocket, parameter: name)
    }

    //MARK: - Items
    func fetchLocation(id: Int) async throws -> Location {
        try await fetchAndDecode(from: .location, parameter: id)
    }

    func fetchLocation(name: String) async throws -> Location {
        try await fetchAndDecode(from: .location, parameter: name)
    }

    func fetchLocationArea(id: Int) async throws -> LocationArea {
        try await fetchAndDecode(from: .locationArea, parameter: id)
    }

    func fetchLocationArea(name: String) async throws -> LocationArea {
        try await fetchAndDecode(from: .locationArea, parameter: name)
    }

    func fetchPalParkArea(id: Int) async throws -> PalParkArea {
        try await fetchAndDecode(from: .palParkArea, parameter: id)
    }

    func fetchPalParkArea(name: String) async throws -> PalParkArea {
        try await fetchAndDecode(from: .palParkArea, parameter: name)
    }

    func fetchRegion(id: Int) async throws -> Region {
        try await fetchAndDecode(from: .region, parameter: id)
    }

    func fetchRegion(name: String) async throws -> Region {
        try await fetchAndDecode(from: .region, parameter: name)
    }

    //MARK: - Machines
    func fetchMachine(id: Int) async throws -> Machine {
        try await fetchAndDecode(from: .machine, parameter: id)
    }

    //MARK: - Moves
    func fetchMove(id: Int) async throws -> Move {
        try await fetchAndDecode(from: .move, parameter: id)
    }

    func fetchMove(name: String) async throws -> Move {
        try await fetchAndDecode(from: .move, parameter: name)
    }

    func fetchMoveAilment(id: Int) async throws -> MoveAilment {
        try await fetchAndDecode(from: .moveAilment, parameter: id)
    }

    func fetchMoveAilment(name: String) async throws -> MoveAilment {
        try await fetchAndDecode(from: .moveAilment, parameter: name)
    }

    func fetchMoveBattleStyle(id: Int) async throws -> MoveBattleStyle {
        try await fetchAndDecode(from: .moveBattleStyle, parameter: id)
    }

    func fetchMoveBattleStyle(name: String) async throws -> MoveBattleStyle {
        try await fetchAndDecode(from: .moveBattleStyle, parameter: name)
    }

    func fetchMoveCategory(id: Int) async throws -> MoveCategory {
        try await fetchAndDecode(from: .moveCategory, parameter: id)
    }

    func fetchMoveCategory(name: String) async throws -> MoveCategory {
        try await fetchAndDecode(from: .moveCategory, parameter: name)
    }

    func fetchMoveDamageClass(id: Int) async throws -> MoveDamageClass {
        try await fetchAndDecode(from: .moveDamageClass, parameter: id)
    }

    func fetchMoveDamageClass(name: String) async throws -> MoveDamageClass {
        try await fetchAndDecode(from: .moveDamageClass, parameter: name)
    }

    func fetchMoveLearnMethod(id: Int) async throws -> MoveLearnMethod {
        try await fetchAndDecode(from: .moveLearnMethod, parameter: id)
    }

    func fetchMoveLearnMethod(name: String) async throws -> MoveLearnMethod {
        try await fetchAndDecode(from: .moveLearnMethod, parameter: name)
    }

    func fetchMoveTarget(id: Int) async throws -> MoveTarget {
        try await fetchAndDecode(from: .moveTarget, parameter: id)
    }

    func fetchMoveTarget(name: String) async throws -> MoveTarget {
        try await fetchAndDecode(from: .moveTarget, parameter: name)
    }

    //MARK: - Pokemon
    func fetchAbility(id: Int) async throws -> Ability {
        try await fetchAndDecode(from: .ability, parameter: id)
    }

    func fetchAbility(name: String) async throws -> Ability {
        try await fetchAndDecode(from: .ability, parameter: name)
    }

    func fetchCharacteristic(id: Int) async throws -> Characteristic {
        try await fetchAndDecode(from: .characteristic, parameter: id)
    }

    func fetchEggGroup(id: Int) async throws -> EggGroup {
        try await fetchAndDecode(from: .eggGroup, parameter: id)
    }

    func fetchEggGroup(name: String) async throws -> EggGroup {
        try await fetchAndDecode(from: .eggGroup, parameter: name)
    }

    func fetchGender(id: Int) async throws -> Gender {
        try await fetchAndDecode(from: .gender, parameter: id)
    }

    func fetchGender(name: String) async throws -> GrowthRate {
        try await fetchAndDecode(from: .gender, parameter: name)
    }

    func fetchGrowthRate(id: Int) async throws -> GrowthRate {
        try await fetchAndDecode(from: .growthRate, parameter: id)
    }

    func fetchGrowthRate(name: String) async throws -> Gender {
        try await fetchAndDecode(from: .growthRate, parameter: name)
    }

    func fetchNature(id: Int) async throws -> Nature {
        try await fetchAndDecode(from: .nature, parameter: id)
    }

    func fetchNature(name: String) async throws -> Nature {
        try await fetchAndDecode(from: .nature, parameter: name)
    }

    func fetchPokeathlonStat(id: Int) async throws -> PokeathlonStat {
        try await fetchAndDecode(from: .pokeathlonStat, parameter: id)
    }

    func fetchPokeathlonStat(name: String) async throws -> PokeathlonStat {
        try await fetchAndDecode(from: .pokeathlonStat, parameter: name)
    }

    func fetchPokemon() async throws -> NamedAPIResourceList<Pokemon> {
        try await fetchAndDecode(from: .pokemon)
    }

    func fetchPokemon(id: Int) async throws -> Pokemon {
        try await fetchAndDecode(from: .pokemon, parameter: id)
    }

    func fetchPokemon(name: String) async throws -> Pokemon {
        try await fetchAndDecode(from: .pokemon, parameter: name)
    }

    func fetchPokemonLocationArea(id: Int) async throws -> [LocationAreaEncounter] {
        try await fetchAndDecode(from: Endpoints.pokemon.url.appendingPathComponent("\(id)").appendingPathComponent("encounters"))
    }

    func fetchPokemonLocationArea(name: String) async throws -> [LocationAreaEncounter] {
        try await fetchAndDecode(from: Endpoints.pokemon.url.appendingPathComponent("\(name)").appendingPathComponent("encounters"))
    }

    func fetchPokemonColor(id: Int) async throws -> PokemonColor {
        try await fetchAndDecode(from: .pokemonColor, parameter: id)
    }

    func fetchPokemonColor(name: String) async throws -> PokemonColor {
        try await fetchAndDecode(from: .pokemonColor, parameter: name)
    }

    func fetchPokemonForm(id: Int) async throws -> PokemonForm {
        try await fetchAndDecode(from: .pokemonForm, parameter: id)
    }

    func fetchPokemonForm(name: String) async throws -> PokemonForm {
        try await fetchAndDecode(from: .pokemonForm, parameter: name)
    }

    func fetchPokemonHabitat(id: Int) async throws -> PokemonHabitat {
        try await fetchAndDecode(from: .pokemonHabitat, parameter: id)
    }

    func fetchPokemonHabitat(name: String) async throws -> PokemonHabitat {
        try await fetchAndDecode(from: .pokemonHabitat, parameter: name)
    }

    func fetchPokemonShape(id: Int) async throws -> PokemonShape {
        try await fetchAndDecode(from: .pokemonShape, parameter: id)
    }

    func fetchPokemonShape(name: String) async throws -> PokemonShape {
        try await fetchAndDecode(from: .pokemonShape, parameter: name)
    }

    func fetchPokemonSpecies(id: Int) async throws -> PokemonSpecies {
        try await fetchAndDecode(from: .pokemonSpecies, parameter: id)
    }

    func fetchPokemonSpecies(name: String) async throws -> PokemonSpecies {
        try await fetchAndDecode(from: .pokemonSpecies, parameter: name)
    }

    func fetchStat(id: Int) async throws -> Stat {
        try await fetchAndDecode(from: .stat, parameter: id)
    }

    func fetchStat(name: String) async throws -> Stat {
        try await fetchAndDecode(from: .stat, parameter: name)
    }

    func fetchTypes() async throws -> NamedAPIResourceList<`Type`> {
        try await fetchAndDecode(from: .type)
    }

    func fetchType(id: Int) async throws -> `Type` {
        try await fetchAndDecode(from: .type, parameter: id)
    }

    func fetchType(name: String) async throws -> `Type` {
        try await fetchAndDecode(from: .type, parameter: name)
    }

    //MARK: - API Resources
    func fetchAPIResource<T: BaseAPIResource>(_ resource: T) async throws -> T.Resource {
        return try await fetchAndDecode(from: resource.url)
    }
    

    //MARK: - Helper Methods
    internal func fetchAndDecode<T: Decodable>(_ type: T.Type = T.self, from url: URL) async throws -> T {
        let request = URLRequest(url: url)

        let (data, response) = try await session.data(for: request)
        guard let response = response as? HTTPURLResponse else { throw APIError.networkError }

        switch response.statusCode {
            case 200...299:
#if DEBUG
                return try! jsonDecoder.decode(T.self, from: data)
#else
                return try jsonDecoder.decode(T.self, from: data)
#endif
            default:
                throw APIError.invalidServerResponse(response.statusCode)
        }
    }

    private func fetchAndDecode<T: Decodable>(_ type: T.Type = T.self, from endpoint: Endpoints, parameter: LosslessStringConvertible? = nil) async throws -> T {
        var request: URLRequest
        if let parameter = parameter {
            request = URLRequest(url: endpoint.url.appendingPathComponent(parameter.description))
        } else {
            request = URLRequest(url: endpoint.url)
        }

        let (data, response) = try await session.data(for: request)
        guard let response = response as? HTTPURLResponse else { throw APIError.networkError }

        switch response.statusCode {
            case 200...299:
#if DEBUG
                return try! jsonDecoder.decode(T.self, from: data)
#else
                return try jsonDecoder.decode(T.self, from: data)
#endif
            default:
                throw APIError.invalidServerResponse(response.statusCode)
        }
    }
}
