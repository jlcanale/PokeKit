//
//  ItemHolderPokemon.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ItemHolderPokemon: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let pokemon: NamedAPIResource<Pokemon>
    public let versionDetails: ItemHolderPokemonVersionDetail
}
