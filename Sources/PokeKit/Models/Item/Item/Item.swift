//
//  Item.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Item: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let cost: Int
    public let flingPower: Int?
    public let flingEffect: NamedAPIResource<ItemFlingEffect>?
    public let attributes: [NamedAPIResource<ItemAttribute>]
    public let category: NamedAPIResource<ItemCategory>
    public let effectEntries: [VerboseEffect]
    public let flavorTextEntries: [VersionGroupFlavorText]
    public let gameIndices: [GenerationGameIndex]
    public let names: [Name]
    public let sprites: ItemSprites
    public let heldByPokemon: [ItemHolderPokemon]
    public let babyTriggerFor: [APIResource<EvolutionChain>]?
    public let machines: [MachineVersionDetail]
}
