//
//  ItemSprites.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ItemSprites: Codable, Identifiable {
    public var id: URL { `default` }
    public let `default`: URL
}
