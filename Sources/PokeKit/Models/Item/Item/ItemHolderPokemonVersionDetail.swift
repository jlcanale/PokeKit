//
//  ItemHolderPokemonVersionDetail.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ItemHolderPokemonVersionDetail: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let rarity: Int
    public let version: NamedAPIResource<Version>
}
