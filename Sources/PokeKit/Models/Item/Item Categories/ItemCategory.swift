//
//  ItemCategory.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ItemCategory: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let items: [NamedAPIResource<Item>]
    public let names: [Name]
    public let pocket: NamedAPIResource<ItemPocket>
}
