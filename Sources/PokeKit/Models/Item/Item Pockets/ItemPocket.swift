//
//  ItemPocket.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ItemPocket: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let categories: [NamedAPIResource<ItemCategory>]
    public let names: [Name]
}
