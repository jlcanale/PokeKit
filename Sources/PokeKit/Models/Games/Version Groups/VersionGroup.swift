//
//  VersionGroup.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct VersionGroup: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let order: Int
    public let generation: NamedAPIResource<Generation>
    public let moveLearnMethods: [NamedAPIResource<MoveLearnMethod>]
    public let pokedexes: [NamedAPIResource<Pokedex>]
    public let regions: [NamedAPIResource<Region>]
    public let versions: [NamedAPIResource<Version>]

    public enum VersionGroup: String, Codable, CodingKey {
        case redBlue = "red-blue"
        case yellow = "yellow"
        case goldSilver = "gold-silver"
        case crystal = "crystal"
        case rubySapphire = "ruby-sapphire"
        case emerald = "emerald"
        case fireRedLeafGreen = "firered-leafgreen"
        case diamondPearl = "diamond-pearl"
        case platinum = "platinum"
        case heartGoldSoulSilver = "heartgold-soulsilver"
        case blackWhite = "black-white"
        case colosseum = "colosseum"
        case xd = "xd"
        case black2White2 = "black-2-white-2"
        case xy = "x-y"
        case omegaRubyAlphaSapphire = "omega-ruby-alpha-sapphire"
        case sunMoon = "sun-moon"
        case ultraSunUltraMoon = "ultra-sun-ultra-moon"
        case letsGo = "lets-go"
        case swordShield = "sword-shield"
    }
}


