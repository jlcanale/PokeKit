//
//  Generation.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Generation: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let abilities: [NamedAPIResource<Ability>]
    public let names: [Name]
    public let mainRegion: NamedAPIResource<Region>
    public let moves: [NamedAPIResource<Move>]
    public let pokemonSpecies: [NamedAPIResource<PokemonSpecies>]
    public let types: [NamedAPIResource<`Type`>]
    public let versionGroups: [NamedAPIResource<VersionGroup>]

    public enum Generation: String, Codable, CodingKey {
        case generation1 = "generation-i"
        case generation2 = "generation-ii"
        case generation3 = "generation-iii"
        case generation4 = "generation-iv"
        case generation5 = "generation-v"
        case generation6 = "generation-vi"
        case generation7 = "generation-vii"
        case generation8 = "generation-viii"
    }
}

