//
//  PokemonEntry.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct PokemonEntry: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let entryNumber: Int
    public let pokemonSpecies: NamedAPIResource<PokemonSpecies>
}
