//
//  Pokedex.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Pokedex: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let isMainSeries: Bool
    public let descriptions: [Description]
    public let names: [Name]
    public let pokemonEntries: [PokemonEntry]
    public let region: NamedAPIResource<Region>?
    public let versionGroups: [NamedAPIResource<VersionGroup>]
}
