//
//  Version.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Version: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let names: [Name]
    public let versionGroup: NamedAPIResource<VersionGroup>
}
