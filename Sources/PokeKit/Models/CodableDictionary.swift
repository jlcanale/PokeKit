//
//  CodableDictionary.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct CodableDictionary<Key: Hashable, Value: Codable>: Codable, Equatable where Key: CodingKey {


    public static func == (lhs: CodableDictionary<Key, Value>, rhs: CodableDictionary<Key, Value>) -> Bool {
        lhs.decoded.keys == rhs.decoded.keys
    }

    let decoded: [Key: Value]

    init(_ decoded: [Key: Value]) {
        self.decoded = decoded
    }

    public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: Key.self)

        decoded = Dictionary(uniqueKeysWithValues:
            try container.allKeys.lazy.map {
                (key: $0, value: try container.decode(Value.self, forKey: $0))
            }
        )
    }

    subscript(key: Key) -> Value? {
        get {
            return decoded[key]
        }
    }

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: Key.self)

        for (key, value) in decoded {
            try container.encode(value, forKey: key)
        }
    }
}

extension CodableDictionary: Hashable where Value: Hashable {}

