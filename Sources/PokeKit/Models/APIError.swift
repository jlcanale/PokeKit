//
//  APIError.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

/// Enum representing the types of Error that the `BGGAPI` can throw
public enum APIError: Error, LocalizedError {

    /// Error when a malformed URL is used.
    case badURL

    /// Error when the server returns a status code other than 200.
    ///
    /// The associated value contains the actual error from the server.
    case invalidServerResponse(Int)

    /// Error when the server returns no data.
    case noDataReturned

    /// Error when the server does not reutrn enough data to parse.
    case insufficientDataReturned

    /// Error when the server was unreachable
    case networkError

    /// Error when the JSON could not be parsed.
    ///
    /// The associated value contains the actual error from the `JSONDecoder`
    case jsonFailure(Error)

    /// Generic error to be used when none of the other error cases are valid.
    case genericError(Error)

    /// Retreive the localized description for this error.
    public var errorDescription: String? {
        switch self {
            case .badURL:
                return "There was a problem forming the URL."
            case .invalidServerResponse(let statusCode):
                return "The server responded with an invalid status code: \(statusCode)"
            case .noDataReturned:
                return "The API call was successful, but no data was returned."
            case .insufficientDataReturned:
                return """
            The API call was successful, and data was returned,
            but the data returned wasn't sufficient to complete the call.
            """
            case .jsonFailure(let error):
                return "There was a problem decoding the JSON data: \(error.localizedDescription)"
            case .genericError(let error):
                return "An Error Occured: \(error.localizedDescription)"
            case .networkError:
                return "A network error occured."
        }
    }
}
