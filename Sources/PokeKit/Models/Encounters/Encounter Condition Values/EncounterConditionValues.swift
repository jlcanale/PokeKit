//
//  EncounterConditionValues.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct EncounterConditionValues: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let condition: NamedAPIResource<EncounterCondition>
    public let names: [Name]
}
