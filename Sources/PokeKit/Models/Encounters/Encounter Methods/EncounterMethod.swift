//
//  EncounterMethod.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct EncounterMethod: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let order: Int
    public let names: [Name]
}
