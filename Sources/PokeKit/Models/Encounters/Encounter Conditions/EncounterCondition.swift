//
//  EncounterCondition.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct EncounterCondition: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let names: [Name]
    public let values: [NamedAPIResource<EncounterConditionValues>]
}
