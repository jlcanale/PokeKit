//
//  MoveAilment.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct MoveAilment: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let moves: [NamedAPIResource<Move>]
    public let names: [Name]
}
