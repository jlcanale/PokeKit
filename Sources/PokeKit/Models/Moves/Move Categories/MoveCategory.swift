//
//  MoveCategory.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct MoveCategory: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let moves: [NamedAPIResource<Move>]
    public let descriptions: [Description]
}
