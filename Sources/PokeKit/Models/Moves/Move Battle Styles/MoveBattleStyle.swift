//
//  MoveBattleStyle.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct MoveBattleStyle: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let names: [Name]
}
