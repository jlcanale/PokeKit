//
//  ContestComboSets.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ContestComboSets: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let normal: ContestComboDetail
    public let `super`: ContestComboDetail
}
