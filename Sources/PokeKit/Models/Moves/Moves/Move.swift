//
//  Move.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Move: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let accuracy: Int
    public let effectChance: Int
    public let pp: Int
    public let priority: Int
    public let power: Int
    public let contestCombos: ContestComboSets
    public let contestType: NamedAPIResource<ContestType>
    public let contestEffect: APIResource<ContestEffect>
    public let damageClass: NamedAPIResource<MoveDamageClass>
    public let effectEntries: [VerboseEffect]
    public let effectChanges: [AbilityEffectChange]
    public let learnedByPokemon: NamedAPIResource<Pokemon>
    public let flavorTextEntries: [MoveFlavorText]
    public let generation: NamedAPIResource<Generation>
    public let machines: [MachineVersionDetail]
    public let meta: [MoveMetaData]
    public let names: [Name]
    public let pastValues: [PastMoveStatValues]
    public let statChanges: [MoveStatChange]
    public let superContestEffect: APIResource<SuperContestEffect>
    public let target: NamedAPIResource<MoveTarget>
    public let type: NamedAPIResource<`Type`>
}
