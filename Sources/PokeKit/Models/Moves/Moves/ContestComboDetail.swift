//
//  ContestComboDetail.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ContestComboDetail: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let useBefore: NamedAPIResource<Move>
    public let useAfter: NamedAPIResource<Move>
}
