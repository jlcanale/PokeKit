//
//  MoveStatChange.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct MoveStatChange: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let change: Int
    public let stat: NamedAPIResource<Stat>
}
