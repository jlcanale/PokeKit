//
//  MoveMetaData.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct MoveMetaData: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let ailment: NamedAPIResource<MoveAilment>
    public let category: NamedAPIResource<MoveCategory>
    public let minHits: Int?
    public let maxHits: Int?
    public let minTurns: Int?
    public let maxTurns: Int?
    public let drain: Int
    public let healing: Int
    public let critRate: Int
    public let ailmentChance: Int
    public let flinchChance: Int
    public let statChance: Int
}
