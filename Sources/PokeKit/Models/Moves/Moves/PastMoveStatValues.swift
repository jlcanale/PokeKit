//
//  PastMoveStatValues.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct PastMoveStatValues: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let accuracy: Int
    public let effectChance: Int
    public let power: Int
    public let pp: Int
    public let effectEntries: [VerboseEffect]
    public let type: NamedAPIResource<`Type`>
    public let versionGroup: NamedAPIResource<VersionGroup>
}
