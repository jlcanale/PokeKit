//
//  Machine.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Machine: Codable, Identifiable {
    public let id: Int
    public let item: NamedAPIResource<Item>
    public let move: NamedAPIResource<Move>
    public let versionGroup: NamedAPIResource<VersionGroup>
}
