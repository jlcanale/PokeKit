//
//  VersionGameIndex.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct VersionGameIndex: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let gameIndex: Int
    public let version: NamedAPIResource<Version>
}
