//
//  VersionGroupFlavorText.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct VersionGroupFlavorText: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let text: String
    public let language: NamedAPIResource<Language>
    public let versionGroup: NamedAPIResource<VersionGroup>
}
