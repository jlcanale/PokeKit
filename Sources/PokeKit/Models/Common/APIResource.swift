//
//  APIResource.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct APIResource<T: Codable>: Codable, Hashable, BaseAPIResource {
    public typealias Resource = T
    public let url: URL
}

