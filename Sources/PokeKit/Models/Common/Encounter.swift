//
//  Encounter.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Encounter: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let minLevel: Int
    public let maxLevel: Int
    public let conditionValues: [NamedAPIResource<EncounterConditionValues>]
    public let chance: Int
    public let method: NamedAPIResource<EncounterMethod>
}
