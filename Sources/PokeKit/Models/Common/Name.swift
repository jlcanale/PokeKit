//
//  Name.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Name: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let name: String
    public let language: NamedAPIResource<Language>
}
