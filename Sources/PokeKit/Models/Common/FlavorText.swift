//
//  FlavorText.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct FlavorText: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let flavorText: String
    public let language: NamedAPIResource<Language>
    public let version: NamedAPIResource<Version>?
}
