//
//  VersionEncounterDetail.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct VersionEncounterDetail: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let version: NamedAPIResource<Version>
    public let maxChance: Int
    public let encounterDetails: [Encounter]
}
