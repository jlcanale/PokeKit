//
//  Language.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Language: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let official: Bool
    public let iso639: String
    public let iso3166: String
    public let names: [Name]
}
