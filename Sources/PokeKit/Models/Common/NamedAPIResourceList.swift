//
//  NamedAPIResourceList.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct NamedAPIResourceList<T: Codable>: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public private(set) var count: Int
    public private(set) var next: URL?
    public private(set) var previous: URL?
    public private(set) var results: [NamedAPIResource<T>]
}

@available(iOS 15.0.0, macOS 12.0.0, *)
extension NamedAPIResourceList {
    mutating func fetchNextPage() async throws {
        guard let next = next else { return }

        let nextResults = try await PokeAPI.shared.fetchAndDecode(Self.self, from: next)
        self.next = nextResults.next
        self.previous = nextResults.previous
        self.results.append(contentsOf: nextResults.results)
    }
}
