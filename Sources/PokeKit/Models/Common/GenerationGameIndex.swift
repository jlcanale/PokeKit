//
//  GenerationGameIndex.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct GenerationGameIndex: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let gameIndex: Int
    public let generation: NamedAPIResource<Generation>
}
