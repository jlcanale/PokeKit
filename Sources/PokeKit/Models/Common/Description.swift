//
//  Description.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Description: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let description: String
    public let language: NamedAPIResource<Language>
}
