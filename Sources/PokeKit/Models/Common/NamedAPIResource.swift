//
//  NamedAPIResource.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct NamedAPIResource<T: Codable>: Codable, Hashable, BaseAPIResource {
    public typealias Resource = T
    public let name: String
    public let url: URL
}
