//
//  MoveStatAffectSets.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct MoveStatAffectSets: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let increase: [MoveStatAffect]
    public let decrease: [MoveStatAffect]
}
