//
//  Stat.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Stat: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let gameIndex: Int
    public let isBattleOnly: Bool
    public let affectingMoves: MoveStatAffectSets
    public let affectingNatures: NatureStatAffectSets
    public let characteristics: [APIResource<Characteristic>]
    public let moveDamageClass: NamedAPIResource<MoveDamageClass>?
    public let names: [Name]
}
