//
//  MoveStatAffect.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct MoveStatAffect: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let change: Int
    public let move: NamedAPIResource<Move>
}
