//
//  NatureStatAffectSets.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct NatureStatAffectSets: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let increase: [NamedAPIResource<Nature>]
    public let decrease: [NamedAPIResource<Nature>]
}
