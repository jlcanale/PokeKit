//
//  PalParkEncounterArea.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PalParkEncounterArea: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let baseScore: Int
    public let rate: Int
    public let area: NamedAPIResource<PalParkArea>
}
