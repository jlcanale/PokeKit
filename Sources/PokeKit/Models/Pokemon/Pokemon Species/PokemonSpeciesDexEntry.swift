//
//  PokemonSpeciesDexEntry.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonSpeciesDexEntry: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let entryNumber: Int
    public let pokedex: NamedAPIResource<Pokedex>
}
