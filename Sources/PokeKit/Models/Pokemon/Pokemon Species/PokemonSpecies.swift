//
//  PokemonSpecies.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonSpecies: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let order: Int
    public let genderRate: Int
    public let captureRate: Int
    public let baseHappiness: Int
    public let isBaby: Bool
    public let isLegendary: Bool
    public let isMythical: Bool
    public let hatchCounter: Int
    public let hasGenderDifferences: Bool
    public let formsSwitchable: Bool
    public let growthRate: NamedAPIResource<GrowthRate>
    public let pokedexNumbers: [PokemonSpeciesDexEntry]
    public let eggGroups: [NamedAPIResource<EggGroup>]
    public let color: NamedAPIResource<PokemonColor>
    public let shape: NamedAPIResource<PokemonShape>
    public let evolvesFromSpecies: NamedAPIResource<PokemonSpecies>?
    public let evolutionChain: APIResource<EvolutionChain>
    public let habitat: NamedAPIResource<PokemonHabitat>
    public let generation: NamedAPIResource<Generation>
    public let names: [Name]
    public let palParkEncounters: [PalParkEncounterArea]
    public let flavorTextEntries: [FlavorText]
    public let formDescriptions: [Description]
    public let genera: [Genus]
    public let varieties: [PokemonSpeciesVariety]
}
