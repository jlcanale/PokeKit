//
//  PokemonSpeciesVariety.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonSpeciesVariety: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let isDefault: Bool
    public let pokemon: NamedAPIResource<Pokemon>
}
