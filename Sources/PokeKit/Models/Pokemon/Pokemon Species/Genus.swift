//
//  Genus.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Genus: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let genus: String
    public let language: NamedAPIResource<Language>
}
