//
//  PokemonShape.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonShape: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let awesomeNames: [AwesomeName]
    public let names: [Name]
    public let pokemonSpecies: [NamedAPIResource<PokemonSpecies>]
}
