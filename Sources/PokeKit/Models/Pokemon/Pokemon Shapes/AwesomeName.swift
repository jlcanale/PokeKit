//
//  AwesomeName.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct AwesomeName: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let awesomeName: String
    public let language: NamedAPIResource<Language>
}
