//
//  LocationAreaEncounter.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct LocationAreaEncounter: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let locationArea: NamedAPIResource<LocationArea>
    public let versionDetails: [VersionEncounterDetail]
}
