//
//  AbilityFlavorText.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct AbilityFlavorText: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let flavorText: String
    public let language: NamedAPIResource<Language>
    public let versionGroup: NamedAPIResource<VersionGroup>
}
