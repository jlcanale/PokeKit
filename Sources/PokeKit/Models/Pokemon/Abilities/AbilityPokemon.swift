//
//  AbilityPokemon.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct RawAbilityPokemon: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let isHidden: Bool
    public let slot: Int
    public let pokemon: NamedAPIResource<Pokemon>
}
