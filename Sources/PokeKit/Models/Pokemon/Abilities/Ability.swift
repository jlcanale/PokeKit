//
//  Ability.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Ability: Codable, Identifiable {
    public let effectChanges: [AbilityEffectChange]
    public let effectEntries: [VerboseEffect]
    public let flavorTextEntries: [AbilityFlavorText]
    public let generation: NamedAPIResource<Generation>
    public let id: Int
    public let isMainSeries: Bool
    public let name: String
    public let names: [Name]
}

