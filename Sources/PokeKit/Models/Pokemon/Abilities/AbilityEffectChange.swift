//
//  AbilityEffectChange.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct AbilityEffectChange: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let effectEntries: [Effect]
    public let versionGroup: NamedAPIResource<VersionGroup>
}
