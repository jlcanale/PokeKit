//
//  Characteristic.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Characteristic: Codable, Identifiable {
    public let id: Int
    public let geneModulo: Int
    public let possibleValues: [Int]
}
