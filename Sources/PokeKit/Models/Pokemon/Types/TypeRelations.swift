//
//  Type.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct TypeRelations: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let noDamageTo: [NamedAPIResource<`Type`>]
    public let halfDamageTo: [NamedAPIResource<`Type`>]
    public let doubleDamageTo: [NamedAPIResource<`Type`>]
    public let noDamageFrom: [NamedAPIResource<`Type`>]
    public let halfDamageFrom: [NamedAPIResource<`Type`>]
    public let doubleDamageFrom: [NamedAPIResource<`Type`>]
}
