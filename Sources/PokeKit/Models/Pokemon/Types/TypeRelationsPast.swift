//
//  TypeRelationsPast.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct TypeRelationsPast: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let generation: NamedAPIResource<Generation>
    public let damageRelations: [TypeRelations]
}
