//
//  File.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct `Type`: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let damageRelations: TypeRelations
    public let pastDamageRelations: [TypeRelationsPast]
    public let gameIndices: [GenerationGameIndex]
    public let generation: NamedAPIResource<Generation>
    public let moveDamageClass: NamedAPIResource<MoveDamageClass>
    public let names: [Name]
    public let pokemon: [TypePokemon]
    public let moves: [NamedAPIResource<Move>]
}
