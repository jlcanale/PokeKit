//
//  Nature.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Nature: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let decreasedStat: NamedAPIResource<Stat>?
    public let increasedStat: NamedAPIResource<Stat>?
    public let hatesFlavor: NamedAPIResource<Berry>?
    public let likesFlavor: NamedAPIResource<Berry>?
    public let pokeathlonStatChanges: [NatureStatChange]
    public let moveBattleStylePreferences: [MoveBattleStylePreference]
    public let names: [Name]
}
