//
//  NatureStatChange.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct NatureStatChange: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let maxChange: Int
    public let pokeathlonStat: NamedAPIResource<PokeathlonStat>
}
