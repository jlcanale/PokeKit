//
//  MoveBattleStylePreference.swift
//
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct MoveBattleStylePreference: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let lowHpPreference: Int
    public let highHpPreference: Int
    public let moveBattleStyle: NamedAPIResource<MoveBattleStyle>
}
