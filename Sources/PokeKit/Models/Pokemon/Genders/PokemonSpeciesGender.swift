//
//  PokemonSpeciesGender.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonSpeciesGender: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let rate: Int
    public let pokemonSpecies: NamedAPIResource<PokemonSpecies>
}
