//
//  Gender.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Gender: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let pokemonSpeciesDetails: [PokemonSpeciesGender]
    public let requiredForEvolution: [NamedAPIResource<PokemonSpecies>]
}
