//
//  PokemonColor.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonColor: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let names: [Name]
    public let pokemonSpecies: [NamedAPIResource<PokemonSpecies>]
}
