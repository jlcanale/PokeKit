//
//  PokemonHeldItem.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonHeldItem: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let item: NamedAPIResource<Item>
    public let versionDetails: [PokemonHeldItemVersion]
}
