//
//  PokemonSprites.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonSprites: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let backDefault: URL?
    public let backFemale: URL?
    public let backShiny: URL?
    public let backShinyFemale: URL?
    public let frontDefault: URL?
    public let frontFemale: URL?
    public let frontShiny: URL?
    public let frontShinyFemale: URL?
    public let other: CodableDictionary<PokemonSpriteOtherType, PokemonSprites>?
    public let versions: CodableDictionary<Generation.Generation, CodableDictionary<VersionGroup.VersionGroup, PokemonSprites>>?
}
