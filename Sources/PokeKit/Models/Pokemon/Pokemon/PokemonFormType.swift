//
//  PokemonFormType.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonFormType: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let slot: Int
    public let type: NamedAPIResource<`Type`>
}
