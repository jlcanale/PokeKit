//
//  PokemonMoveVersion.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonMoveVersion: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let moveLearnMethod: NamedAPIResource<MoveLearnMethod>
    public let versionGroup: NamedAPIResource<VersionGroup>
    public let levelLearnedAt: Int
}
