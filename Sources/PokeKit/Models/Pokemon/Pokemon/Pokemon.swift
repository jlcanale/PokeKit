//
//  Pokemon.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Pokemon: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let baseExperience: Int
    public let height: Int
    public let isDefault: Bool
    public let order: Int
    public let weight: Int
    public let abilities: [PokemonAbility]
    public let forms: [NamedAPIResource<PokemonForm>]
    public let gameIndices: [VersionGameIndex]
    public let heldItems: [PokemonHeldItem]
    public let locationAreaEncounters: URL
    public let moves: [PokemonMove]
    public let pastTypes: [PokemonTypePast]
    public let species: NamedAPIResource<PokemonSpecies>
    public let sprites: PokemonSprites
    public let stats: [PokemonStat]
    public let types: [PokemonType]
}



