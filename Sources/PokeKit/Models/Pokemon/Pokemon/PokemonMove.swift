//
//  PokemonMove.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonMove: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let move: NamedAPIResource<Move>
    public let versionGroupDetails: [PokemonMoveVersion]
}


