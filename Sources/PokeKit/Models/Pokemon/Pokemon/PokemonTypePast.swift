//
//  PokemonTypePast.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonTypePast: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let generation: NamedAPIResource<Generation>
    public let types: [PokemonType]
}
