//
//  PokemonAbility.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonAbility: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let ability: NamedAPIResource<Ability>
    public let isHidden: Bool
    public let slot: Int
}


