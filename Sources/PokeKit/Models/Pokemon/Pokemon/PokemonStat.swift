//
//  RawStat.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonStat: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let baseStat: Int
    public let effort: Int
    public let stat: NamedAPIResource<Stat>
}
