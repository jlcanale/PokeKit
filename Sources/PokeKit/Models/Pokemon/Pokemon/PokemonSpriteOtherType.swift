//
//  RawPokemonSpriteOtherType.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public enum PokemonSpriteOtherType: String, Codable, CodingKey {
    case dreamWorld = "dream_world"
    case home = "home"
    case officialArtwork = "official-artwork"
}
