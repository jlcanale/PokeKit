//
//  NaturePokeathlonStatAffect.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct NaturePokeathlonStatAffect: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let maxChange: Int
    public let nature: NamedAPIResource<Nature>
}
