//
//  NaturePokeathlonStatAffectSets.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct NaturePokeathlonStatAffectSets: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let increase: [NaturePokeathlonStatAffect]
    public let decrease: [NaturePokeathlonStatAffect]
}
