//
//  GrowthRate.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct GrowthRate: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let formula: String
    public let descriptions: [Description]
    public let levels: [GrowthRateExperienceLevel]
    public let pokemonSpecies: [NamedAPIResource<PokemonSpecies>]
}
