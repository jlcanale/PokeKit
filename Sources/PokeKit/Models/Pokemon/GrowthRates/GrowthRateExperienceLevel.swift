//
//  GrowthRateExperienceLevel.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct GrowthRateExperienceLevel: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let level: Int
    public let experience: Int
}
