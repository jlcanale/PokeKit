//
//  RawPokemonForm.swift
//  PokeAPI
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonForm: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let order: Int
    public let formOrder: Int
    public let isDefault: Bool
    public let isBattleOnly: Bool
    public let isMega: Bool
    public let formName: String
    public let pokemon: NamedAPIResource<Pokemon>
    public let types: [PokemonFormType]
    public let sprites: PokemonFormSprite
    public let versionGroup: NamedAPIResource<VersionGroup>
    public let names: [Name]
    public let formNames: [Name]
}



