//
//  PokemonFormSprite.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct PokemonFormSprite: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let frontDefault: URL
    public let frontShiny: URL
    public let backDefault: URL
    public let backShiny: URL
}
