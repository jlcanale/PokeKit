//
//  BerryFlavorMap.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct BerryFlavorMap: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let potency: Int
    public let flavor: NamedAPIResource<BerryFlavor>
}
