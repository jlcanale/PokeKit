//
//  Berry.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct Berry: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let growthTime: Int
    public let maxHarvest: Int
    public let naturalGiftPower: Int
    public let size: Int
    public let smoothness: Int
    public let soilDryness: Int
    public let firmness: NamedAPIResource<BerryFirmness>
    public let flavors: [BerryFlavorMap]
    public let item: NamedAPIResource<Item>
    public let naturalGiftType: NamedAPIResource<`Type`>
}
