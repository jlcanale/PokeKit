//
//  FlavorBerryMap.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct FlavorBerryMap: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let potency: Int
    public let berry: NamedAPIResource<Berry>
}
