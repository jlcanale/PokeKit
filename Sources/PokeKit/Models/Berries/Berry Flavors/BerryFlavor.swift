//
//  BerryFlavor.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct BerryFlavor: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let berries: [FlavorBerryMap]
    public let contestType: NamedAPIResource<ContestType>
    public let names: [Name]
}
