//
//  BerryFirmness.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct BerryFirmness: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let berries: [NamedAPIResource<Berry>]
    public let names: [Name]
}
