//
//  Location.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Location: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let region: NamedAPIResource<Region>
    public let names: [Name]
    public let gameIndices: [GenerationGameIndex]
    public let areas: [NamedAPIResource<LocationArea>]
}
