//
//  Region.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct Region: Codable, Identifiable {
    public let id: Int
    public let locations: [NamedAPIResource<Location>]
    public let name: String
    public let names: [Name]
    public let mainGeneration: NamedAPIResource<Generation>
    public let pokedexes: [NamedAPIResource<Pokedex>]
    public let versionGroups: [NamedAPIResource<VersionGroup>]
}
