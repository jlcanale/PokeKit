//
//  PalParkEncounterSpecies.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct PalParkEncounterSpecies: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let baseScore: Int
    public let rate: Int
    public let pokemonSpecies: NamedAPIResource<PokemonSpecies>
}
