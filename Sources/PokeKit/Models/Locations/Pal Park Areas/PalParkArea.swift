//
//  PalParkArea.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct PalParkArea: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let names: [Name]
    public let pokemonEncounters: [PalParkEncounterSpecies]
}
