//
//  EncounterVersionDetails.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct EncounterVersionDetails: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let rate: Int
    public let version: NamedAPIResource<Version>
}
