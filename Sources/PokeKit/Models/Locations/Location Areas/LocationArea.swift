//
//  LocationArea.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct LocationArea: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let gameIndex: Int
    public let encounterMethodRates: [EncounterMethodRate]
    public let location: NamedAPIResource<Location>
    public let names: [Name]
    public let pokemonEncounters: [PokemonEncounter]
}
