//
//  EncounterMethodRate.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct EncounterMethodRate: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let encounterMethod: NamedAPIResource<EncounterMethod>
    public let versionDetails: [EncounterVersionDetails]
}
