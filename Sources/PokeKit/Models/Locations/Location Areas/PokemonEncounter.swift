//
//  PokemonEncounter.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct PokemonEncounter: Codable {
    public let pokemon: NamedAPIResource<Pokemon>
    public let versionDetails: [VersionEncounterDetail]
}
