//
//  SuperContestEffect.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct SuperContestEffect: Codable, Identifiable {
    public let id: Int
    public let appeal: Int
    public let flavorTextEntries: [FlavorText]
    public let moves: [NamedAPIResource<Move>]
}
