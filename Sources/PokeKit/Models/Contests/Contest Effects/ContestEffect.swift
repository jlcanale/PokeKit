//
//  ContestEffect.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct ContestEffect: Codable, Identifiable {
    public let id: Int
    public let appeal: Int
    public let jam: Int
    public let effectEntries: [Effect]
    public let flavorTextEntries: [FlavorText]
}
