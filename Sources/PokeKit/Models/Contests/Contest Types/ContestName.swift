//
//  ContestName.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct ContestName: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let name: String
    public let color: String
    public let language: NamedAPIResource<Language>
}
