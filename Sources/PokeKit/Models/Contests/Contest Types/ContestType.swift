//
//  ContestType.swift
//  
//
//  Created by Joseph Canale on 11/5/21.
//

import Foundation

public struct ContestType: Codable, Identifiable {
    public let id: Int
    public let name: String
    public let berryFlavor: NamedAPIResource<BerryFlavor>
    public let names: [ContestName]
}
