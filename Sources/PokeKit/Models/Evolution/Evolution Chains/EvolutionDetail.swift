//
//  EvolutionDetail.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct EvolutionDetail: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let item: NamedAPIResource<Item>?
    public let trigger: NamedAPIResource<EvolutionTrigger>
    public let gender: Int?
    public let heldItem: NamedAPIResource<Item>?
    public let knownMove: NamedAPIResource<Move>?
    public let knownMoveType: NamedAPIResource<`Type`>?
    public let location: NamedAPIResource<Location>?
    public let minLevel: Int?
    public let minHappiness: Int?
    public let minBeauty: Int?
    public let minAffection: Int?
    public let needsOverworldRain: Bool
    public let partySpecies: NamedAPIResource<PokemonSpecies>?
    public let partyType: NamedAPIResource<`Type`>?
    public let relativePhysicalStats: Int?
    public let timeOfDay: String
    public let tradeSpecies: NamedAPIResource<PokemonSpecies>?
    public let turnUpsideDown: Bool
}
