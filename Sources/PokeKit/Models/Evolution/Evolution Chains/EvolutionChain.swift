//
//  EvolutionChain.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct EvolutionChain: Codable, Identifiable {
    public let id: Int
    public let babyTriggerItem: NamedAPIResource<Item>?
    public let chain: ChainLink
}
