//
//  ChainLink.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import Foundation

public struct ChainLink: Codable, Identifiable, Hashable {
    public var id: Int { self.hashValue }
    public let isBaby: Bool
    public let species: NamedAPIResource<PokemonSpecies>
    public let evolutionDetails: [EvolutionDetail]
    public let evolvesTo: [ChainLink]
}
