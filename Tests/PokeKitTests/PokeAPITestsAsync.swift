//
//  PokeAPITestsAsync.swift
//  
//
//  Created by Joseph Canale on 11/6/21.
//

import XCTest
@testable import PokeKit

@available(iOS 15.0, macOS 12.0.0, *)
final class PokeAPITestsAsync: XCTestCase {
    let api = PokeAPI.shared

    //MARK: - Berries
    func testFetchBerry() async throws {
        let fetched = try await api.fetchBerry(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchBerryFirmness() async throws {
        let fetched = try await api.fetchBerryFirmness(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchFlavor() async throws {
        let fetched = try await api.fetchBerryFlavor(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Contests
    func testFetchContestType() async throws {
        let fetched = try await api.fetchContestType(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchContestEffect() async throws {
        let fetched = try await api.fetchContestEffect(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchSuperContestEffect() async throws {
        let fetched = try await api.fetchSuperContestEffect(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Encounters
    func testFetchEncounterMethod() async throws {
        let fetched = try await api.fetchEncounterMethod(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchEncounterCondition() async throws {
        let fetched = try await api.fetchEncounterCondition(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchEncounterConditionValue() async throws {
        let fetched = try await api.fetchEncounterConditionValue(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Evolutions
    func testFetchEvolutionChain() async throws {
        let fetched = try await api.fetchEvolutionChain(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchEvolutionTrigger() async throws {
        let fetched = try await api.fetchEvolutionTrigger(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Games
    func testFetchGeneration() async throws {
        let fetched = try await api.fetchGeneration(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokedex() async throws {
        let fetched = try await api.fetchPokedex(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchVersion() async throws {
        let fetched = try await api.fetchVersion(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchVersionGroup() async throws {
        let fetched = try await api.fetchVersionGroup(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Items
    func testFetchItem() async throws {
        let fetched = try await api.fetchItem(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchItemAttribute() async throws {
        let fetched = try await api.fetchItemAttribute(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchItemCategory() async throws {
        let fetched = try await api.fetchItemCategory(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchItemFlingEffect() async throws {
        let fetched = try await api.fetchItemFlingEffect(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchItemPocket() async throws {
        let fetched = try await api.fetchItemPocket(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Locations
    func testFetchLocation() async throws {
        let fetched = try await api.fetchItem(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchLocationArea() async throws {
        let fetched = try await api.fetchLocationArea(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPalParkArea() async throws {
        let fetched = try await api.fetchPalParkArea(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchRegion() async throws {
        let fetched = try await api.fetchRegion(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Machines
    func testFetchMachine() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Moves
    func testFetchMove() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchMoveAilment() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchMoveBattleStyle() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchMoveCategory() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchMoveDamageClass() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchMoveLearnMethod() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchMoveTarget() async throws {
        let fetched = try await api.fetchMachine(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    //MARK: - Pokemon
    func testFetchAbility() async throws {
        let fetched = try await api.fetchAbility(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchCharacteristic() async throws {
        let fetched = try await api.fetchCharacteristic(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchEggGroup() async throws {
        let fetched = try await api.fetchEggGroup(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchGender() async throws {
        let fetched = try await api.fetchGender(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchGrowthRate() async throws {
        let fetched = try await api.fetchGrowthRate(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchNature() async throws {
        let fetched = try await api.fetchNature(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokeathlonStat() async throws {
        let fetched = try await api.fetchPokeathlonStat(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokemon() async throws {
        let fetched = try await api.fetchPokemon(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokemonLocationArea() async throws {
        let _ = try await api.fetchPokemonLocationArea(id: 1)
        XCTAssertTrue(true)
    }

    func testFetchPokemonColor() async throws {
        let fetched = try await api.fetchPokemonColor(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokemonForm() async throws {
        let fetched = try await api.fetchPokemonForm(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokemonHabitat() async throws {
        let fetched = try await api.fetchPokemonHabitat(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokemonShape() async throws {
        let fetched = try await api.fetchPokemonShape(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPokemonSpecies() async throws {
        let fetched = try await api.fetchPokemonSpecies(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchStat() async throws {
        let fetched = try await api.fetchStat(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchType() async throws {
        let fetched = try await api.fetchType(id: 1)
        XCTAssertEqual(fetched.id, 1)
    }

    func testFetchPagedPokemon() async throws {
        var results = try await api.fetchPokemon()
        XCTAssertEqual(results.results.count, 20)
        try await results.fetchNextPage()
        XCTAssertEqual(results.results.count, 40)
    }

    func testFetchPagedType() async throws {
        var results = try await api.fetchTypes()
        XCTAssertEqual(results.results.count, 20)
        try await results.fetchNextPage()
        XCTAssertEqual(results.results.count, 20)
    }
}
